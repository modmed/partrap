This is an interpreter for the ParTraP language, proposed in the [MODMED project][modmed].
It checks that execution traces satisfy formal properties given in a high-level language.

# Build and run

The tool is written in Haskell and you will need the Haskell tool [stack][stack] to compile it.

Compile:

```
stack setup
stack build
```

Execute:

```
stack exec partrap -- traces/example.json
```

You should now be facing a prompt where you can enter properties.

# Trace input format

The input trace must be formatted in JSON, where the root element is a sequence of objects.
Each object represents an event, which must contain at least a key `id` associated with a string.
For instance, the following is a valid trace:

```json
[
	{ "id": "EventA", "time": 1 },
	{ "id": "EventB", "time": 2, "value": 42 },
	{ "id": "EventC", "time": 4, "data": { "valid": true } }
]
```

The key `time` is also mandatory when verifying timed properties.

# Example

Let us consider the trace given as example above.
We want to check that an occurrence of the event `EventA` prevents any occurrence of the event `EventC` with valid data.
After loading the example trace in the interpreter, we can enter the property:

```
> EventA prevents EventC ec where ec.data.valid
```

which, as expected, is not satisfied:

```
KO:
in the scope starting after EventA[],
the event EventC[data=[valid=True]] should not have occurred
```

For more examples, please consult the [language proposal][proposal].

# Language definition

This section is an overview of the language grammar together with its informal semantics.
Its complete definition and formal semantics is available in the [MODMED language proposal][proposal].

## Identifiers, expressions and durations

Identifiers are used to name events and variables.

```javascript
<ident> ::= (<letter> | '_') (<digit> | <letter> | '_')*
```

Expressions allow constraining and extracting the parameters carried by the events.

```javascript
<expr> ::= <ident>
         | <float>
         | <integer>
         | <string>
         | (<expr>)
         | <expr> '.' <ident>
         | 'not' <expr>
         | <expr> ('<' | '<=' | '==' | '>=' | '>') <ident>
         | <expr> ('and' | 'or') <ident>
         | <expr> ('+' | '-') <ident>
```

Durations are used to restrict the range of action of operators in time.

```javascript
<duration> ::= <expr> ('ms' | 's' | 'min' | 'h' | 'd')
```

## Events

Events constructs allow selecting events from a trace.

```javascript
<event> ::= <ident> [<ident> ['where' <expr>]]
```

Events are designated by their name, such as in where `A` is the name of the event.
Additionally, an event can be bound to a variable `x` like in `A x`.
In this case, it is also possible to add a condition on the event thanks to the `where` construct: `A x where c`.
This expression describes the set of events having the type `A` and respecting the condition `c` when bound to the variable `x`.

## Patterns

Patterns are the base components of properties.
They describe and rule the occurrences of events in the trace.

```javascript
<pattern> ::= 'absence_of' <event>
            | 'occurrence_of' <event>
            | <event> 'followed_by' <event> ['within' <duration>]
            | <event> 'precedes' <event> ['within' <duration>]
            | <event> 'prevents' <event> ['for' <duration>]
```

The first unary pattern is `occurrence_of A`, which requires *at least* on occurrence of the event `A`.
The second unary pattern is simply its dual: `absence_of A`.

There are three binary patterns: `A followed_by B`, `A precedes B` and `A prevents B`, each optionally annotated with a time duration.
`A followed_by B` holds if every occurrence of the event `A` is *eventually* followed by an occurrence of the event `B`.
If no event `A` occurs, the patterns is trivially satisfied.
The meaning of two remaining patterns is obvious.
The range of action of all three binary patterns can additionally be restricted in time by suffixing them with a duration constraint.

## Scopes

Scopes are a mean to designate the range of a trace where a property should hold.
They are delimited by events.

```javascript
<scope> ::= ['within' <duration>] ('after' | 'before') ('each' | 'first' | 'last') <event>
          | 'between' <event> 'and' <event>
          | 'since' <event> 'until' <event>
```

The scopes we propose can be classified according to their arity, *i.e.* the number of events they involve.

Unary scopes, are the basic building blocks.
Note that all scopes are strict, i.e., delimiter events are not included in the interval they define.
The "each" variants may describe multiple intervals and happen to be useful combinators.
Their range of ation may additionally be restricted in time by prefixing them with a duration constraint.
We also included the two binary scopes originally suggested by Dwyer *et. al.*: `between A and B`, and its weak variant `since A until B`.

## Properties

Properties bring patterns and scopes together.

```javascript
<property> ::= <pattern>
             | <scope> ',' <property>
             | 'forall' <ident> 'in' <expr> ',' <property>
             | '(' <property> ')'
             | 'not' <property>
             | <property> ('and' | 'or') <property>
```

The first rule allow deriving bare patterns as valid properties.
It is the only non-recursive rule, which makes deriving a property not containing at least one pattern impossible.
The remaining rules allow combining patterns with others constructs.

The second rule refine the scope of a trace where a property should hold.
In the absence of scope restriction, a property must be satisfied by the whole trace.
An important consequence of this rule is that scopes can be nested.
For instance, `after last A, after each B, P` will hold if and only if `P` holds after each `B` occurring after the last occurrence of `A`.
All events used in a scope that are bound to a variable name will be made available in the underlying property.
For instance, the property `after first A v where v.x != 0, P` will evaluate `P` on the scope starting after the first event `A` with a non-null `x` parameter, and in a environment where the value of the variable `v` is this first event.

The third rule relies on the fact that event parameters can be arrays.
It ensures that a property holds for each element of such an array.
The remaining rules are classical boolean connectors applied to properties.

The following properties are typical examples:

- `between A a and B b where a.x == b.x, absence_of C or D precedes E`
- `after first A a, forall v in a.s, occurrence_of C c where c.x == v`

# Special commands

In addition to properties, the interpreter will recognize 3 commands:

- `:events` prints all the events from the given traces
- `:trace i` prints the `i`-th trace
- `:coverage p` prints coverage information for the property `p` on the given traces

# Limitations

The current implementation is a work in progress and you may encounter some limitations:

- Expressions only include simple relations
- Somewhat terse error messages
- No batch mode for properties

# Run the test suite

```
stack test
```

[modmed]: https://sites.google.com/a/minmaxmedical.com/modmed/
[stack]: http://haskellstack.org/
[proposal]: https://drive.google.com/file/d/0B2EA_O7JMI4nYzlpZ3Zsa1dFTVk/view

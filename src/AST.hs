module AST where

import Data.List (intercalate)
import Data.Text (Text)
import qualified Data.Text as Text
import Expr

type Strict = Bool

data Prop
  = Scoped Scope Prop
  | Forall Text Expr Prop
  | Exists Text Expr Prop
  | PropPattern Pattern
  | PropOr Prop Prop
  | PropAnd Prop Prop
  | PropNot Prop
  | Given Strict Occurrence EventDesc Prop
  deriving (Eq)

instance Show Prop where
  show (Scoped s p) = show s ++ ", " ++ show p
  show (Given strict o e p) = "given" ++ (if strict then "1 " else " ") ++ show o ++ " " ++ show e ++ ", " ++ show p
  show (Forall x s p) = "forall " ++ Text.unpack x ++ " " ++ show s ++ ", " ++ show p
  show (Exists x s p) = "exists " ++ Text.unpack x ++ " " ++ show s ++ ", " ++ show p
  show (PropPattern p) = show p
  show (PropOr p1 p2) = "(" ++ show p1 ++ ") or (" ++ show p2 ++ ")"
  show (PropAnd p1 p2) = "(" ++ show p1 ++ ") and (" ++ show p2 ++ ")"
  show (PropNot p1) = "not (" ++ show p1 ++ ")"

data Scope
  = After Strict Occurrence EventDesc (Maybe Duration)
  | Before Strict Occurrence EventDesc (Maybe Duration)
  | Between EventDesc EventDesc
  | SinceUntil EventDesc EventDesc
  deriving (Eq)

instance Show Scope where
  show (After s o e md) = showMaybeDur "within " md ++ "after" ++ (if s then "1 " else " ") ++ show o ++ " " ++ show e
  show (Before s o e md) = showMaybeDur "within " md ++ "before" ++ (if s then "1 " else " ") ++ show o ++ " " ++ show e
  show (Between e1 e2) = "between " ++ show e1 ++ " and " ++ show e2
  show (SinceUntil e1 e2) = "since " ++ show e1 ++ " until " ++ show e2

data Occurrence
  = First
  | Last
  | Each
  deriving (Eq)

instance Show Occurrence where
  show First = "first"
  show Last = "last"
  show Each = "each"

data Pattern
  = Absence EventDesc
  | Existence EventDesc
  | Response EventDesc EventDesc (Maybe Duration)
  | Precedence EventDesc EventDesc (Maybe Duration)
  | Prevention EventDesc EventDesc (Maybe Duration)
  deriving (Eq)

instance Show Pattern where
  show (Absence e) = "absence_of " ++ show e
  show (Existence e) = "occurrence_of " ++ show e
  show (Response e1 e2 md) = show e1 ++ " followed_by " ++ show e2 ++ showMaybeDur " for " md
  show (Precedence e1 e2 md) = show e1 ++ " precedes " ++ show e2 ++ showMaybeDur " for " md
  show (Prevention e1 e2 md) = show e1 ++ " prevents " ++ show e2 ++ showMaybeDur " for " md


data EventDesc
  = SimpleEvent { name :: Text, var :: Maybe Text, guard :: Maybe Expr }
  deriving (Eq)

instance Show EventDesc where
  show (SimpleEvent name mVar mGuard) = Text.unpack name ++ varStr ++ guardStr
    where
      varStr = maybe "" (\var -> " " ++ Text.unpack var) mVar
      guardStr = maybe "" (\guard -> " where " ++ show guard) mGuard

data Duration = Duration { expr :: Expr, unit :: TimeUnit }
  deriving (Eq)

instance Show Duration where
  show d = show (expr d) ++ show (unit d)

data TimeUnit
  = Milliseconds
  | Seconds
  | Minutes
  | Hours
  | Days
  deriving (Eq)

instance Show TimeUnit where
  show (Milliseconds) = "ms"
  show (Seconds) = "s"
  show (Minutes) = "min"
  show (Hours) = "h"
  show (Days) = "d"

showMaybeDur _      Nothing = ""
showMaybeDur prefix (Just d) = prefix ++ show (expr d) ++ show (unit d) ++ " "

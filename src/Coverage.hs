module Coverage (coverage) where

import Data.Either (isRight)
import Data.List (transpose)
import AST
import DNF
import Trace
import Interpreter.Offline

coverage' :: [Prop] -> Trace -> [Bool]
coverage' ps t = map (isRight . satisfies t) ps

-- | Compute the coverage of a property over a trace.
-- | Each pair is composed of a sub-property that can satisfy the given property and the percentage of traces that covered it.e :: Prop -> [Trace] -> [(Prop, Float)]
coverage p ts = zip ps percents
  where
    percents = map (\l -> (fromIntegral $ sum l) / (fromIntegral $ length ts)) $ transpose intResults
    intResults = map (map fromEnum) results
    results = map (coverage' ps) ts
    ps = toDNF p

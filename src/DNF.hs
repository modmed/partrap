module DNF (toDNF) where

import AST

-- | Performs a single rewrite
step :: Prop -> Prop

-- Double negation elimination
step (PropNot (PropNot p)) = p

-- De Morgan's laws
step (PropNot (PropOr p q)) = PropAnd (PropNot p) (PropNot q)
step (PropNot (PropAnd p q)) = PropOr (PropNot p) (PropNot q)

-- Quantifiers duality
step (PropNot (Forall t x p)) = Exists t x (PropNot p)
step (PropNot (Exists t x p)) = Forall t x (PropNot p)

-- Absence/Existence duality
step (PropNot (PropPattern (Existence x))) = PropPattern (Absence x)
step (PropNot (PropPattern (Absence x))) = PropPattern (Existence x)

-- Scopes distributes over disjunction and conjunction
step (Scoped s@(After _ First _ _) (PropOr p q)) = PropOr (Scoped s p) (Scoped s q)
step (Scoped s@(After _ Last _ _) (PropOr p q)) = PropOr (Scoped s p) (Scoped s q)
step (Scoped s@(Before _ _ _ _) (PropAnd p q)) = PropAnd (Scoped s p) (Scoped s q)

-- Break down non-strict scopes
step (Scoped (After False o e md) p) = PropOr (PropPattern (Absence e)) (Scoped (After True o e md) p)
step (Scoped (Before False o e md) p) = PropOr (PropPattern (Absence e)) (Scoped (Before True o e md) p)

-- The negation of a strict scope is a non-strict scope with the subproperty negated
step (PropNot (Scoped (After True o@First e md) p)) = Scoped (After False o e md) (PropNot p)
step (PropNot (Scoped (Before True o@First e md) p)) = Scoped (Before False o e md) (PropNot p)
step (PropNot (Scoped (After True o@Last e md) p)) = Scoped (After False o e md) (PropNot p)
step (PropNot (Scoped (Before True o@Last e md) p)) = Scoped (Before False o e md) (PropNot p)

-- The negation of a non-strict scope is a strict scope with the subproperty negated
-- This rule is not necessary but it cuts steps and noise
--step (PropNot (Scoped (After False o e md) p)) = Scoped (After True o e md) (PropNot p)
--step (PropNot (Scoped (Before False o e md) p)) = Scoped (Before True o e md) (PropNot p)

-- Conjuction distributes over disjuction
step (PropAnd (PropOr p1 p2) p3) = PropOr (PropAnd p1 p3) (PropAnd p2 p3)
step (PropAnd p1 (PropOr p2 p3)) = PropOr (PropAnd p1 p2) (PropAnd p1 p3)

-- Decompose higher level constructs
step (PropPattern (Response e1 e2 md)) = Scoped (After False Each e1 md) $ PropPattern $ Existence e2
step (PropPattern (Precedence e1 e2 md)) = Scoped (Before False Each e2 md) $ PropPattern $ Existence e1
step (PropPattern (Prevention e1 e2 md)) = Scoped (After False Each e1 md) $ PropPattern $ Absence e2

step (Scoped (Between e1 e2) p) = p'
  where p' = Scoped (After False Each e1 Nothing) $ Scoped (Before False First e2 Nothing) p

step (Scoped (SinceUntil e1 e2) p) = (p1 `PropAnd` (p21 `PropOr` p22))
  where p1 = Scoped (Between e1 e2) p
        p21 = Scoped (After True Last e2 Nothing) (Scoped (After False Each e1 Nothing) p)
        p22 = PropPattern (Absence e2) `PropAnd` (Scoped (After False Each e1 Nothing) p)

-- If no match at top level, try recursively
step (PropNot p) = PropNot (step p)
step (PropOr p1 p2) = PropOr (step p1) (step p2)
step (PropAnd p1 p2) = PropAnd (step p1) (step p2)
step (Scoped s p) = Scoped s (step p)
step (Forall t x p) = Forall t x (step p)
step (Exists t x p) = Exists t x (step p)

-- No rewrite is possible
step p = p


-- | Split the disjunctions of a property
breakDisjuctions :: Prop -> [Prop]
breakDisjuctions (PropOr p1 p2) = concat [breakDisjuctions p1, breakDisjuctions p2]
breakDisjuctions p = [p]

-- | Transform a property into disjunctive normal form
toDNF :: Prop -> [Prop]
toDNF p
  | p' == p = breakDisjuctions p
  | otherwise = toDNF $ step p'
  where p' = step p

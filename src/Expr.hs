module Expr (Expr, Env, eval, parseExpr) where

import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as Text
import Control.Applicative
import Text.Parsec hiding ((<|>), many)
import Text.Parsec.Expr
import Text.Parsec.Language
import Text.Parsec.String
import qualified Text.Parsec.Token as Token
import Data.Functor.Identity (Identity)
import Value

type Env = Map Text Value

data Expr
  = Ident Text
  | ConstTrue
  | ConstFloat Double
  | ConstString Text
  | Not Expr
  | Binary BinOp Expr Expr
  | Access Expr Text
  | FunCall Text [Expr]
  deriving (Eq)

instance Show Expr where
  show (Ident i) = Text.unpack i
  show ConstTrue = "true"
  show (ConstFloat f) = show f
  show (ConstString s) = Text.unpack s
  show (Not e) = "not (" ++ show e ++ ")"
  show (Binary op e1 e2) = "(" ++ show e1 ++ ") " ++ show op ++ " (" ++ show e2 ++ ")"
  show (Access e p) = show e ++ "." ++ Text.unpack p
  show (FunCall n args) = Text.unpack n ++ "(" ++ intercalate ", " (map show args) ++ ")"

data BinOp = Sub | Add | Or | And | Equals | LessThan | LTE deriving (Eq)

instance Show BinOp where
  show Sub = "-"
  show Add = "+"
  show And = "and"
  show Or = "or"
  show Equals = "=="
  show LessThan = "<"
  show LTE = "<="

reservedNames = ["absence_of", "where", "occurrence_of", "each", "first", "last",
                 "before", "after", "precedes", "followed_by", "forall", "exists", "in", "prevents",
                 "since", "until", "between", "given", "ms", "s", "min", "h", "d",
                 "within", "for"]

lexer = Token.makeTokenParser
  emptyDef
    { Token.identStart      = letter
    , Token.identLetter     = alphaNum <|> char '_' <?> "identifier"
    , Token.reservedNames   = reservedNames
    , Token.reservedOpNames = [".", "-", "==", "<=", "<", ">", ">=", "not", "and", "or"]
    }

identifier     = Text.pack <$> Token.identifier lexer
reservedOp     = Token.reservedOp lexer
parens         = Token.parens     lexer
naturalOrFloat = Token.naturalOrFloat      lexer
stringLiteral  = Text.pack <$> Token.stringLiteral lexer
commaSep       = Token.commaSep lexer

parseExpr :: Parser Expr
parseExpr = buildExpressionParser opTable term

term = parens parseExpr
  <|> callOrIdent
  <|> (ConstFloat . either fromInteger id <$> naturalOrFloat)
  <|> (ConstString <$> stringLiteral)
  where
    callOrIdent = do
      name <- identifier
      (FunCall name) <$> (parens $ commaSep parseExpr) <|> return (Ident name)

opTable :: OperatorTable String () Identity Expr
opTable =
  [ [ postfix $ flip Access <$ reservedOp "." <*> identifier]
  , [ prefix $ Not <$ reservedOp "not"]
  , [ binary Sub "-"
    , binary Add "+"]
  , [ binary Equals "=="
    , binary LessThan "<"
    , binary LTE "<="
    , rbinary LessThan ">"
    , rbinary LTE ">=" ]
  , [ binary And "and"
    , binary Or "or"]
  ]
  where binary op name = Infix (Binary op <$ reservedOp name) AssocLeft
        rbinary op name = Infix (flip (Binary op) <$ reservedOp name) AssocLeft
        prefix  p = Prefix  . chainl1 p $ return       (.)
        postfix p = Postfix . chainl1 p $ return (flip (.))


-- | Imported functions that can be called in expressions
prelude :: Map Text ([Value] -> Value)
prelude = Map.fromList
  [ ("dist", \[p1, p2] ->
       let unvalf (ValFloat f) = f
           unvall (ValList l) = map unvalf l
       in ValFloat $ sqrt $ sum $ map (^2) $ zipWith (-) (unvall p1) (unvall p2))
  ]

eval env (Access e f) =
  case eval env e of
    ValRecord r ->
      case Map.lookup f r of
        Just v -> v
        Nothing -> error $ "field not found: " ++ Text.unpack f
    _ -> error "type error"

eval env (FunCall name args) =
  case Map.lookup name prelude of
    Just f -> f $ map (eval env) args
    Nothing -> error $ "function " ++ Text.unpack name ++ " not found"

eval _ (ConstFloat f) =
  ValFloat f

eval _ (ConstTrue) =
  ValBool True

eval _ (ConstString s) =
  ValString s

eval env (Ident i) =
  case Map.lookup i env of
    Just v -> v
    Nothing -> error $ "variable " ++ Text.unpack i ++ " not found"

eval env (Not e) =
  case eval env e of
    ValBool b -> ValBool $ not b
    _ -> error "type error"

eval env (Binary And e1 e2) =
  case (eval env e1, eval env e2) of
    (ValBool True, ValBool True) -> ValBool True
    (ValBool _, ValBool _) -> ValBool False
    _ -> error "type error"

eval env (Binary Or e1 e2) =
  case (eval env e1, eval env e2) of
    (ValBool False, ValBool False) -> ValBool False
    (ValBool _, ValBool _) -> ValBool True
    _ -> error "type error"

eval env (Binary Equals e1 e2) =
  ValBool $ eval env e1 == eval env e2 -- TODO: type check

eval env (Binary LessThan e1 e2) =
  case (eval env e1, eval env e2) of
    (ValFloat i1, ValFloat i2) -> ValBool $ i1 < i2
    _ -> error "type error in `<`"

eval env (Binary LTE e1 e2) =
  case (eval env e1, eval env e2) of
    (ValFloat i1, ValFloat i2) -> ValBool $ i1 <= i2
    _ -> error "type error in `<=`"

eval env (Binary Sub e1 e2) =
  case (eval env e1, eval env e2) of
    (ValFloat i1, ValFloat i2) -> ValFloat $ i1 - i2
    _ -> error "type error in `-`"

eval env (Binary Add e1 e2) =
  case (eval env e1, eval env e2) of
    (ValFloat i1, ValFloat i2) -> ValFloat $ i1 + i2
    _ -> error "type error in `-`"

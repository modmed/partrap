{-# LANGUAGE MonadComprehensions #-}

module Interpreter.Offline where

import AST
import Trace
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe
import Data.List
import Control.Applicative
import Control.Arrow
import Control.Monad
import Data.Vector (Vector, (!))
import qualified Data.Vector as V
import Data.Text (Text)
import qualified Data.Text as Text
import Expr
import Value
import Utils (showMap)

type Trace' = Vector Event

type Verdict = Either String ()
type Verdict' = Either Msg ()

data Msg
  = UnexpectedEvent Event EventDesc
  | NotFound EventDesc
  | ForallFailed Text Value Msg
  | ExistsFailed Text
  | InScope Scope Event Env Msg
  | MsgOr Msg Msg
  | MsgNot Prop

--TODO: improve errors messages with time constraints
instance Show Msg where
  show (UnexpectedEvent e ed) =
    -- "the event " ++ show e ++ " should not have occurred (absence of " ++ show ed ++ ")"
    "the event " ++ show e ++ " should not have occurred"
  show (NotFound ed) =
    "an event of type `" ++ show ed ++ "` should have occurred"
  show (ForallFailed x v msg) =
    "for " ++ Text.unpack x ++ " = " ++ show v ++ ",\n" ++ show msg
  show (ExistsFailed x) =
    "exists " ++ Text.unpack x ++ " failed"
  show (InScope (After _ _ _ _) e env msg) =
    "in the scope starting after " ++ show e ++ envStr ++ ",\n" ++ show msg
    where
      envStr = if Map.size env >= 2 then " and the environment " ++ showMap env else ""
  show (InScope (Before _ _ _ _) e env msg) =
    "in the scope ending before " ++ show e ++ envStr ++ ",\n" ++ show msg
    where
      envStr = if Map.size env >= 2 then " and the environment " ++ showMap env else ""
  show (MsgOr m1 m2) = "either " ++ show m1 ++ ", or " ++ show m2
  show (MsgNot p) = "the property `" ++ show p ++ "` should not be satisfied"

relevantEvents :: Prop -> [EventDesc]
relevantEvents (Scoped s p) = relevantEventsScope s ++ relevantEvents p
relevantEvents (Forall _ _ p) = relevantEvents p
relevantEvents (Exists _ _ p) = relevantEvents p
relevantEvents (PropPattern p) = relevantEventsPattern p
relevantEvents (PropOr p1 p2) = relevantEvents p1 ++ relevantEvents p2
relevantEvents (PropAnd p1 p2) = relevantEvents p1 ++ relevantEvents p2

relevantEventsPattern :: Pattern -> [EventDesc]
relevantEventsPattern (Absence e) = [e]
relevantEventsPattern (Existence e) = [e]
relevantEventsPattern (Response e1 e2 _) = [e1, e2]
relevantEventsPattern (Precedence e1 e2 _) = [e1, e2]
relevantEventsPattern (Prevention e1 e2 _) = [e1, e2]

relevantEventsScope :: Scope -> [EventDesc]
relevantEventsScope (After _ _ e _) = [e]
relevantEventsScope (Before _ _ e _) = [e]
relevantEventsScope (Between e1 e2) = [e1, e2]
relevantEventsScope (SinceUntil e1 e2) = [e1, e2]

filterRelevantEvents :: Prop -> Trace' -> Trace'
filterRelevantEvents p t = V.filter hasRelevantName t
  where
    hasRelevantName (Event name _) = name `elem` relevantEventNames
    relevantEventNames = map eventDescName $ relevantEvents p
    eventDescName (SimpleEvent name _ _) = name

satisfies :: Trace -> Prop -> Verdict
satisfies t p = left show $ (V.fromList t, Map.empty) `sat` p

satisfies' :: Trace' -> Prop -> Verdict
satisfies' t p = left show $ (t, Map.empty) `sat` p

satisfies'' :: Trace' -> Prop -> Verdict
satisfies'' t p = left show $ (filterRelevantEvents p t, Map.empty) `sat` p

sat :: (Trace', Env) -> Prop -> Verdict'

sat c (PropPattern pat) = satPattern c pat

sat c (Scoped (Between e1 e2) p) = c `sat` p'
  where p' = Scoped (After False Each e1 Nothing) $ Scoped (Before False First e2 Nothing) p

sat c (Scoped (SinceUntil e1 e2) p) = c `sat` (p1 `PropAnd` (p21 `PropOr` p22))
   where p1 = Scoped (Between e1 e2) p
         p21 = Scoped (After True Last e2 Nothing) (Scoped (After False Each e1 Nothing) p)
         p22 = PropPattern (Absence e2) `PropAnd` (Scoped (After False Each e1 Nothing) p)

sat (t, env) (Scoped s@(Before strict occ e mDur) p)
  | strict && null ms = Left $ NotFound e
  | otherwise = mapM_ scopeCheck ms
    where ms = matches (t, env) occ e
          scopeCheck (i, env') = annErr $ (timeFilter (V.take i t), env') `sat` p
            where annErr = left $ InScope s (t!i) env'
                  timeFilter = case mDur of
                    Nothing -> id
                    Just dur -> from $ t0 - (toSecs env dur)
                  t0 = case Map.lookup "time" $ eventParams (t ! i) of
                    Nothing -> error $ "Missing time parameter for event: " ++ show (t ! i)
                    Just (ValFloat t0) -> t0
                    _ -> error $ "the `time` field should be a number in the event " ++ show e

sat (t, env) (Scoped s@(After strict occ e mDur) p)
  | strict && null ms = Left $ NotFound e
  | otherwise = mapM_ scopeCheck ms
    where ms = matches (t, env) occ e
          scopeCheck (i, env') = annErr $ (timeFilter (V.drop (i+1) t), env') `sat` p
            where annErr = left $ InScope s (t!i) env'
                  timeFilter = case mDur of
                    Nothing -> id
                    Just dur -> upto $ t0 + (toSecs env dur)
                  t0 = case Map.lookup "time" $ eventParams (t ! i) of
                    Nothing -> error $ "Missing time parameter for event: " ++ show (t ! i)
                    Just (ValFloat t0) -> t0
                    _ -> error $ "the `time` field should be a number in the event " ++ show e

sat (t, env) (Given strict occ e p)
  | strict && null ms = Left $ NotFound e
  | otherwise = mapM_ subCheck ms
    where ms = matches (t, env) occ e
          subCheck (i, env') = (t, env') `sat` p

sat (t, env) (Forall x e p) =
  let f v = left (ForallFailed x v) $ (t, Map.insert x v env) `sat` p
  in case eval env e of
       ValList l -> mapM_ f l
       _ -> error "type error in forall"

sat (t, env) (Exists x e p) =
  let rec [] = Left $ ExistsFailed x
      rec (v:vs) =
        case (t, Map.insert x v env) `sat` p of
          Left _ -> rec vs
          Right _ -> Right ()
  in case eval env e of
       ValList l -> rec l
       _ -> error "type error in exists"

sat (t, env) (PropNot p) =
  case ((t, env) `sat` p) of
    Right _ -> Left $ MsgNot p
    Left _ -> Right ()

sat (t, env) (PropOr p1 p2) =
  case ((t, env) `sat` p1, (t, env) `sat` p2) of
    (Right _, _) -> Right ()
    (_, Right _) -> Right ()
    (Left m1, Left m2) -> Left $ MsgOr m1 m2

sat (t, env) (PropAnd p1 p2) =
  case ((t, env) `sat` p1, (t, env) `sat` p2) of
    (Left m1, _) -> Left m1
    (_, Left m2) -> Left m2
    (Right _, Right _) -> Right ()

satPattern :: (Trace', Env) -> Pattern -> Verdict'

satPattern (t, env) (Absence ed@(SimpleEvent name x guard)) =
  case matches (t, env) First ed of
    [] -> Right ()
    (i, _):_ -> Left $ UnexpectedEvent (t!i) ed

satPattern (t, env) (Existence ed@(SimpleEvent name x guard)) =
  case matches (t, env) First ed of
    [] -> Left $ NotFound ed
    _ -> Right ()

satPattern (t, env) (Precedence e1 e2 mDur) =
  (t, env) `sat` (Scoped (Before False Each e2 mDur) $ PropPattern $ Existence e1)

satPattern (t, env) (Response e1 e2 mDur) =
  (t, env) `sat` (Scoped (After False Each e1 mDur) $ PropPattern $ Existence e2)

satPattern (t, env) (Prevention e1 e2 mDur) =
  (t, env) `sat` (Scoped (After False Each e1 mDur) $ PropPattern $ Absence e2)

-- TODO: compare with a vector as result container
matches :: (Trace', Env) -> Occurrence -> EventDesc -> [(Int, Env)]
matches (t, env) occ (SimpleEvent { name = name, var = x, AST.guard = g }) =
  let
    matching = mapMaybe envIfMatch $ occurrences t name
    envIfMatch i =
      let env' = case x of
            Nothing -> env
            Just x -> Map.insert x (ValRecord $ eventParams (t!i)) env
      in case g of
           Nothing -> Just (i, env')
           Just g -> case eval env' g of
             ValBool True -> Just (i, env')
             ValBool False -> Nothing
             _ -> error $ "the condition `" ++ show g ++ "` must have type bool"
  in
    case occ of
      First -> if null matching then [] else [head matching]
      Last -> if null matching then [] else [last matching]
      Each -> matching

occurrences :: Trace' -> Text -> [Int]
occurrences t name = V.toList $ V.findIndices (\(Event name' _) -> name' == name) t

-- TODO: since time is monotonous, we could use a binary search in upto and from
upto :: Double -> Trace' -> Trace'
upto lim = V.takeWhile beforeLim
  where beforeLim e = case Map.lookup "time" (eventParams e) of
          Nothing -> True
          Just (ValFloat t) -> t < lim
          _ -> error $ "the `time` field should be a number in the event " ++ show e

from :: Double -> Trace' -> Trace'
from lim = V.dropWhile beforeLim
  where beforeLim e = case Map.lookup "time" (eventParams e) of
          Nothing -> True
          Just (ValFloat t) -> t <= lim
          _ -> error $ "the `time` field should be a number in the event " ++ show e

toSecs :: Env -> Duration -> Double
toSecs env dur = case unit dur of
  Milliseconds -> v / 1000
  Seconds -> v
  Minutes -> v * 60
  Hours -> v * 60 * 60
  Days -> v * 60 * 60 * 24
  where v = case eval env (expr dur) of
          ValFloat f -> f
          _ -> error $ "the duration `" ++ show (expr dur) ++ "` is not a number"


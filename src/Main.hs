module Main where

import Parser
import Trace
import AST
import Coverage
import Interpreter.Offline
import Data.Map (Map)
import qualified Data.Map as Map
import System.Exit
import System.Environment
import System.Console.Haskeline
import Control.Monad
import Control.Monad.IO.Class
import Data.Char (toLower)
import Data.Maybe (catMaybes)
import Data.List (isPrefixOf, nub, sortOn)
import Data.Either
import Data.Text (Text)
import qualified Data.Text as Text
import Control.Exception as Exc
import System.Directory
import Numeric (showFFloat)

main :: IO ()
main = do
  args <- getArgs
  when (null args) $ putStrLn "error: must provide at least one JSON trace as argument" >> exitFailure
  traces <- forM args $ \a -> do
    trace <- load a
    return (a, trace)
  putStrLn $ "loaded " ++ show (length traces) ++ " traces"
  repl traces

repl :: [(String, Trace)] -> IO ()
repl traces = do
  let
    handleLine line
      | ":events" `isPrefixOf` line = do
          putStr $ Text.unpack $ Text.unlines $ eventNames
          print $ length eventNames
      | ":trace " `isPrefixOf` line = do
          let ws = words line
          if length ws < 2 then do
            putStrLn $ "usage: trace index"
          else do
            let i = read $ ws !! 1
            mapM_ print (snd $ traces !! i)
            print $ length (snd $ traces !! i)
      | ":coverage " `isPrefixOf` line = do
          if null (drop 10 line) then do
            putStrLn $ "usage: coverage property"
          else do
            case parseProp (drop 10 line) of
              Left err -> putStrLn $ "Parse error at " ++ show err
              Right prop -> Exc.catch (printCoverage prop) handleExc
                where handleExc (Exc.ErrorCallWithLocation msg _) = putStrLn $ "error: " ++ msg
      | otherwise =
          case parseProp line of
            Left err -> putStrLn $ "Parse error at " ++ show err
            Right prop -> Exc.catch (satAll prop) handleExc
              where handleExc (Exc.ErrorCallWithLocation msg _) = putStrLn $ "error: " ++ msg

    satAll prop = do
      let satCount n (name, trace) =
            case trace `satisfies` prop of
              Left msg -> putStrLn ("KO " ++ name ++ ":\n" ++ msg) >> putStrLn "" >> return n
              Right _ -> return (n + 1)
      nSat <- foldM satCount 0 traces
      putStrLn $ show nSat ++ "/" ++ show (length traces) ++ " traces satisfied"

    printCoverage prop = do
      let results = coverage prop (map snd traces)
      forM_ (sortOn (show . fst) results) $ \(p, r) -> do
        putStrLn $ showFFloat Nothing r " | " ++ show p
      putStrLn $ showFFloat Nothing (sum $ map snd results) " | total"

    eventNames = nub $ concat $ map (map eventName . snd) traces
    paramNames = nub $ concat $ concatMap (map eventParamNames . snd) traces
    cmdNames = map (':':) ["trace", "events", "coverage"]
    funcNames = [] -- Map.keys prelude
    allNames = reservedNames ++ map Text.unpack eventNames ++ map Text.unpack paramNames ++ cmdNames ++ funcNames

    searchFunc :: String -> [Completion]
    searchFunc str = map simpleCompletion $ filter (isPrefixOf lstr . map toLower) allNames
      where lstr = map toLower str

    replSettings :: Settings IO
    replSettings = Settings
      { historyFile = Nothing
      , complete = completeWord Nothing " \t.," $ return . searchFunc
      , autoAddHistory = True
      }

    loop = do
      maybeLine <- getInputLine "> "
      case maybeLine of
        Nothing -> return ()
        Just line -> liftIO (handleLine line) >> loop

  runInputT replSettings loop

{-
getTracesUnsat :: FilePath -> Prop -> IO [(FilePath, String)]
getTracesUnsat dir prop = catMaybes <$> (listDirectory dir >>= mapM getError)
  where
    getError file = do
      trace <- load $ dir ++ "/" ++ file
      return $ case trace `satisfies` prop of
        Left err -> Just (file, err)
        Right _ -> Nothing
-}

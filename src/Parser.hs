module Parser where

import Text.Parsec hiding ((<|>), many)
import Text.Parsec.Expr
import Text.Parsec.Language
import Text.Parsec.String
import qualified Text.Parsec.Token as Token
import Control.Applicative
import Data.Functor.Identity (Identity)
import Data.Text (Text)
import qualified Data.Text as Text
import AST
import Expr

parseProp :: String -> Either ParseError Prop
parseProp = Text.Parsec.parse (prop <* eof) ""

reservedNames =
  [ "absence_of", "occurrence_of", "precedes", "followed_by", "prevents",
    "before", "after", "each", "first", "last",
    "since", "until", "between", "given",
    "forall", "exists", "in",
    "where",
    "not", "and", "or",
    "within", "for",
    "ms", "s", "min", "h", "d",
    "before1", "after1", "given1"
  ]

languageDef =
  emptyDef
    { Token.identStart      = letter
    , Token.identLetter     = alphaNum <|> char '_' <?> "identifier"
    , Token.reservedNames   = reservedNames
    , Token.reservedOpNames = ["not", "and", "or"]
    }

lexer = Token.makeTokenParser languageDef

identifier = Text.pack <$> Token.identifier lexer
reserved   = Token.reserved   lexer
reservedOp = Token.reservedOp lexer
parens     = Token.parens     lexer
comma      = Token.comma      lexer

prop :: Parser Prop
prop = buildExpressionParser propOperators prop'

prop' :: Parser Prop
prop' = parens prop <|> (PropPattern <$> pattern)

propOperators :: OperatorTable String () Identity Prop
propOperators =
  [ [ Prefix prefixOp ]
  , [ binary PropAnd "and" ]
  , [ binary PropOr "or" ]
  ]
  where
    binary op name = Infix (op <$ reservedOp name) AssocLeft

-- `buildExpressionParser` does not support chains of unary operators of the
-- same precedence. Therefore, we group them together under a chainl1.
prefixOp = chainl1 anyPrefixOp $ return (.)
  where
    anyPrefixOp = choice
      [ PropNot <$ reservedOp "not"
      , Forall <$ reserved "forall" <*> identifier <* reserved "in" <*> parseExpr <* comma
      , Exists <$ reserved "exists" <*> identifier <* reserved "in" <*> parseExpr <* comma
      , Given True <$ reserved "given1" <*> occurrence <*> event <* comma
      , Given False <$ reserved "given" <*> occurrence <*> event <* comma
      , Scoped <$> scope <* comma
      ]

scope :: Parser Scope
scope = afterOrBefore
  <|> (Between <$ reserved "between" <*> event <* reserved "and" <*> event)
  <|> (SinceUntil <$ reserved "since" <*> event <* reserved "until" <*> event)
  where
    afterOrBefore = do
      mDur <- optionMaybe (reserved "within" *> duration)
      constr <- (After True <$ reserved "after1") <|> (Before True <$ reserved "before1")
            <|> (After False <$ reserved "after") <|> (Before False <$ reserved "before")
      o <- occurrence
      e <- event
      return $ constr o e mDur

occurrence :: Parser Occurrence
occurrence = choice $ [First <$ reserved "first", Last <$ reserved "last", Each <$ reserved "each"]

pattern :: Parser Pattern
pattern = choice
  [ Absence <$ reserved "absence_of" <*> event
  , Existence <$ reserved "occurrence_of" <*> event
  , try $ Response <$> event <* reserved "followed_by" <*> event <*> optionMaybe (reserved "within" *> duration)
  , try $ Precedence <$> event <* reserved "precedes" <*> event <*> optionMaybe (reserved "within" *> duration)
  , try $ Prevention <$> event <* reserved "prevents" <*> event <*> optionMaybe (reserved "for" *> duration)
  ]

event :: Parser EventDesc
event = do
  name <- identifier <?> "event type"
  var <- optionMaybe identifier <?> "variable name"
  guard <- optionMaybe $ reserved "where" *> parseExpr
  return $ SimpleEvent { name = name, var = var, guard = guard }


duration :: Parser Duration
duration = do
  expr <- parseExpr
  unit <- timeUnit
  return $ Duration { expr = expr, unit = unit }

timeUnit :: Parser TimeUnit
timeUnit =
      (Milliseconds <$ reserved "ms")
  <|> (Seconds <$ reserved "s")
  <|> (Minutes <$ reserved "min")
  <|> (Hours <$ reserved "h")
  <|> (Days <$ reserved "d")

module Trace where

import Numeric
import Data.List
import Data.Maybe
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.ByteString as B
import qualified Data.Vector as V
import qualified Data.Aeson as JSON
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Scientific
import Data.HashMap.Strict ((!))
import qualified Data.HashMap.Strict as HM
import Control.Arrow
import Value
import Utils (showMap)

data Event = Event Text (Map Text Value) deriving (Eq)

eventName :: Event -> Text
eventName (Event n _) = n

eventParams :: Event -> Map Text Value
eventParams (Event _ p) = p

eventParamNames :: Event -> [Text]
eventParamNames (Event _ p) = Map.keys p

instance Show Event where
  show (Event name params) = Text.unpack name ++ showMap params

type Trace = [Event]

load :: String -> IO Trace
load filename = do
  eJsonObjects <- JSON.eitherDecodeStrict' <$> B.readFile filename
  case eJsonObjects of
    Left err -> error $ "failed to read trace " ++ filename
    Right jsonObjects -> return $ map objectToEvent jsonObjects

objectToEvent :: JSON.Object -> Event
objectToEvent o = Event name params
  where
    ValString name = fromJsonValue $ o ! "id"
    params = Map.fromList $ map (second fromJsonValue) $ HM.toList $ HM.delete "id" o

fromJsonValue :: JSON.Value -> Value
fromJsonValue (JSON.String s) = ValString s
fromJsonValue (JSON.Bool b) = ValBool b
fromJsonValue (JSON.Number s) = ValFloat $ toRealFloat s
fromJsonValue (JSON.Array a) = ValList $ map fromJsonValue $ V.toList a
fromJsonValue (JSON.Object r) = ValRecord $ Map.fromList $ map (second fromJsonValue) $ HM.toList r

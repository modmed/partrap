module Utils where

import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as Text

showMap :: (Show v) => Map Text v -> String
showMap m = "[" ++ (intercalate ", " $ map (\(k, v) -> Text.unpack k ++ "=" ++ show v) $ filter (not . Text.null . fst) $ Map.toList m) ++ "]"

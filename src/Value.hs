module Value where

import Data.List
import Data.Map (Map)
import Data.Text (Text)
import Numeric
import Utils

data Value
  = ValFloat Double
  | ValString Text
  | ValBool Bool
  | ValList [Value]
  | ValRecord (Map Text Value)
  | ValUnknown
  deriving (Eq)

instance Show Value where
  show (ValFloat f)
    | f == fromInteger (round f) = show $ round f
    | otherwise = showFFloat Nothing f ""
  show (ValString s) = show s
  show (ValBool b) = show b
  show (ValList s) = if length s <= 5 then "[" ++ (intercalate ", " (map show s)) ++ "]" else "[...]"
  show (ValRecord r) = showMap r

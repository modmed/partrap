import Parser
import Value
import Trace
import Interpreter.Offline
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Either (isRight, isLeft)
--import Control.Monad (forM_)

import Test.Tasty
import Test.Tasty.HUnit

main = defaultMain $ testGroup "Tests" [{-examples,-} unitTests]

examples = testGroup "examples" $ map (\p -> testProp p [validTrace] [invalidTrace]) props
  where
    props =
      [ "absence_of Temp t where not (20 <= t.t1 and t.t1 < 50)"
      , "occurrence_of EnterState v where v.state == \"redo\""
      , "after each EnterState e, absence_of ActionNext a where a.time - e.time < 1"
      , "EnterState e prevents ActionNext a where a.time - e.time < 1"
      , "EnterState e prevents ActionNext a for 1s"
      , "before each RedoStart r, forall o in r.options, occurrence_of EnterState v where v.state == o"
      , "CameraConnected precedes EnterState e where e.state == \"TrackersConnection\""
      , "after each RegisterTracker rt, TrackerDetected td where td.type == rt.type followed_by DialogConfirm"
      ]

    validTrace =
      [ ev "Temp" [("t1", ValFloat 30)]
      , ev "EnterState" [("time", ValFloat 0), ("state", ValString "init")]
      , ev "CameraConnected" [("time", ValFloat 0)]
      , ev "EnterState" [("time", ValFloat 1), ("state", ValString "TrackersConnection")]
      , ev "ActionNext" [("time", ValFloat 130)]
      , ev "EnterState" [("time", ValFloat 150), ("state", ValString "redo")]
      , ev "RedoStart" [("options", ValList [ValString "init"])]
      , ev "TrackerDetected" [("type", ValFloat 0)]
      , ev "RegisterTracker" [("type", ValFloat 0)]
      , ev "TrackerDetected" [("type", ValFloat 0)]
      , ev "DialogConfirm" []
      ]

    invalidTrace =
      [ ev "Temp" [("t1", ValFloat 300)]
      , ev "EnterState" [("time", ValFloat 0), ("state", ValString "exit")]
      , ev "EnterState" [("time", ValFloat 1), ("state", ValString "TrackersConnection")]
      , ev "ActionNext" [("time", ValFloat 1.1)]
      , ev "RedoStart" [("options", ValList [ValString "init"])]
      , ev "RegisterTracker" [("type", ValFloat 0)]
      , ev "TrackerDetected" [("type", ValFloat 0)]
      ]

unitTests = testGroup "unit tests"
  [ testProp "after first A, occurrence_of B"
    [ []
    , [ev "A" [], ev "B" []]
    ]
    [ [ev "A" []]
    ]

  , testProp "after1 first A, occurrence_of B"
    [ [ev "A" [], ev "B" []]
    ]
    [ []
    , [ev "A" []]
    ]

  , testProp "after first A a, occurrence_of B b where b.x == a.x"
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    , [ev "A" [("x", vf 1)], ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    ]

  , testProp "after first A a, absence_of B b where b.x == a.x"
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    , [ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "A" [("x", vf 0)], ev "B" [("x", vf 2)]]
    , [ev "A" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 1)], ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    ]

  , testProp "before first B b, occurrence_of A a where a.x == b.x"
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 1)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    , [ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)]]
    ]

  , testProp "before first B b, absence_of A a where a.x == b.x"
    [ [ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 2)], ev "B" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 1)], ev "B" [("x", vf 1)]]
    ]

  , testProp "before first B b, after first A a where a.x == b.x, occurrence_of C c where c.x == a.x"
    [ [ev "C" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "C" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "C" [("x", vf 3)], ev "C" [("x", vf 1)], ev "B" [("x", vf 1)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "C" [("x", vf 3)], ev "C" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "A" [("x", vf 2)], ev "C" [("x", vf 2)], ev "B" [("x", vf 2)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "C" [("x", vf 0)], ev "B" [("x", vf 1)]]
    ]

  , testProp "before first C c, before first B b where b.x == c.x, occurrence_of A a where a.x == c.x"
    [ [ev "A" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "B" [("x", vf 1)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)]]
    ]
    [ [ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 1)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "B" [("x", vf 0)], ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    ]

  , testProp "after each A a, occurrence_of B b where b.x == a.x"
    [ []
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    , [ev "B" [("x", vf 0)], ev "A" [("x", vf 1)], ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    ]

  , testProp "before first C c, after each A a where a.x <= c.x, occurrence_of B b where b.x == a.x"
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 1)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 1)], ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)]]
    ]

  , testProp "before each B b, occurrence_of A a where a.x == b.x"
    [ []
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "B" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    , [ev "B" [("x", vf 0)], ev "A" [("x", vf 1)], ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    ]

  , testProp "before first C c, before each B b where b.x <= c.x, occurrence_of A a where a.x == b.x"
    [ []
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 2)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)]]
    , [ev "B" [("x", vf 0)], ev "A" [("x", vf 1)], ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)]]
    ]

  , testProp "after last A a, occurrence_of B b where b.x == a.x"
    [ []
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "A" [("x", vf 1)], ev "B" [("x", vf 1)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 0)]]
    ]

  , testProp "before first C c, after last A a where a.x <= c.x, occurrence_of B b where b.x == a.x"
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 1)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "A" [("x", vf 2)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 1)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "A" [("x", vf 2)], ev "B" [("x", vf 0)], ev "C" [("x", vf 1)]]
    ]

  , testProp "before last B b, occurrence_of A a where a.x == b.x"
    [ []
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    , [ev "A" [("x", vf 1)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)]]
    ]

  , testProp "before first C c, before last B b where b.x <= c.x, occurrence_of A a where a.x == b.x"
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)]]
    ]


  , testProp "before last C c, before last B b where b.x <= c.x, occurrence_of A a where a.x == b.x"
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)], ev "C" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)], ev "C" [("x", vf 0)]]
    ]

  , testProp "not (occurrence_of A or occurrence_of B)"
    [ []
    ]
    [ [ev "B" []]
    , [ev "A" []]
    , [ev "A" [], ev "B" []]
    ]

  , testProp "after first A a, forall v in a.l, occurrence_of B b where b.x == v"
    [ [ev "A" [("l", ValList [vf 0, vf 1])], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "B" [("x", vf 2)]]
    ]
    [ [ev "A" [("l", ValList [vf 0, vf 1])], ev "B" [("x", vf 0)], ev "B" [("x", vf 2)]]
    ]

  , testProp "before first A a, forall v in a.l, occurrence_of B b where b.x == v"
    [ [ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "B" [("x", vf 2)], ev "A" [("l", ValList [vf 0, vf 1])]]
    ]
    [ [ev "B" [("x", vf 0)], ev "B" [("x", vf 2)], ev "A" [("l", ValList [vf 0, vf 1])]]
    ]

  , testProp "given first B b, occurrence_of A a where a.x == b.x"
    [ []
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "B" [("x", vf 0)], ev "A" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)]]
    , [ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "A" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)]]
    , [ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)]]
    ]

  , testProp "given first A a, occurrence_of A b where b.x == a.x"
    [ [ev "A" [("x", vf 0)]]
    ]
    [
    ]

  , testProp "before first C c, given first B b where b.x == c.x, occurrence_of A a where a.x == c.x"
    [ [ev "A" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "B" [("x", vf 1)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 1)]]
    , [ev "B" [("x", vf 0)], ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "B" [("x", vf 1)], ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "A" [("x", vf 0)], ev "C" [("x", vf 0)]]
    ]
    [ [ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "B" [("x", vf 1)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 1)], ev "B" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    ]

  , testProp "given first B b, ((occurrence_of A a where a.x == b.x) and (occurrence_of C c where c.x == b.x))"
    [ [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    ]
    [ [ev "A" [("x", vf 1)], ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    , [ev "A" [("x", vf 0)], ev "B" [("x", vf 0)], ev "C" [("x", vf 1)]]
    ]

  , testProp "given first B b, absence_of C c where c.x == b.x"
    [ [ev "B" [("x", vf 0)], ev "C" [("x", vf 1)]]
    ]
    [ [ev "C" [("x", vf 0)], ev "B" [("x", vf 0)]]
    , [ev "B" [("x", vf 0)], ev "C" [("x", vf 0)]]
    ]

  , testProp "since A until B, occurrence_of C"
    [ []
    , [ev "B" []]
    , [ev "A" [], ev "C" [], ev "B" []]
    , [ev "A" [], ev "C" [], ev "B" [], ev "A" [], ev "C" []]
    , [ev "A" [], ev "C" []]
    ]
    [ [ev "A" [], ev "B" []]
    , [ev "A" []]
    , [ev "A" [], ev "C" [], ev "B" [], ev "A" []]
    ]
  ]

--testProp propStr satTraces unsatTraces = testCaseSteps propStr $ \step -> do
--  step "parse property"
--  case parseProp propStr of
--    Left err -> assertFailure $ show err
--    Right prop -> do
--      step "test property"
--      forM_ satTraces $ \t ->
--        (assertBool "should sat" $ isRight (t `satisfies` prop))
--      forM_ unsatTraces $ \t ->
--        (assertBool "should not sat" $ isLeft (t `satisfies` prop))

testProp propStr satTraces unsatTraces = testGroup propStr $
  case parseProp propStr of
    Left err -> [testCase "parse property" $ assertFailure $ show err]
    Right prop ->
      [ testGroup "sat traces" (map (\t -> testCase (show t) (assertBool "trace does not satisfty the property" $ isRight (t `satisfies` prop))) satTraces)
      , testGroup "unsat traces" (map (\t -> testCase (show t) (assertBool "trace satisfies the property" $ isLeft (t `satisfies` prop))) unsatTraces)
      ]

ev name params = Event name $ Map.fromList params
vf = ValFloat
